"""
How to use the script:
Go to your choreographe installtion directory. bin -> launch naoqi-bin.exe
To see the result in choreographe launch choreographe: connect to -> check "Use fixed port" and "Use fixed IP/hostname" and click "Select"
Run this script, you should then see "Hello, world!" in choreographe.
"""

from naoqi import ALProxy
IP = "localhost"
port = 9559
tts = ALProxy("ALTextToSpeech", IP, port)
tts.say("Hello, world!")