# README #

Intelligent User Experience Engineering Project - Children Robot Interaction

This repository contains codes for Wizard of Oz and master data for question and answer in child robot interaction.

Dependencies:
1. Flask
2. Python => 2.7
3. pynaoqi

To run the Wizard of Oz panel, run the wizardGUI panel and open browser to connect to IP address to tablet interface.

Members:
Bernd Kreynen
Rolf Starre
Timo Fernhout
Romi Kharisnawan