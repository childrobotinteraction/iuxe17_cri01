"""
How to use the script:
Go to your choreographe installtion directory. bin -> launch naoqi-bin.exe
To see the result in choreographe launch choreographe: connect to -> check "Use fixed port" and "Use fixed IP/hostname" and click "Select"
Run this script, you should then see "Hello, world!" in choreographe.
"""
from Tkinter import Tk, Frame, BOTH, Button
from naoqi import ALProxy

IPNao = "localhost"
portNao = 9559

class WizardFrame(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent, background="white")

        self.parent = parent

        self.parent.title("Wizard of Oz")
        self.pack(fill=BOTH, expand=1)
        self.centerWindow()
        talkButton = Button(self, text="Talk",
            command=talk)
        talkButton.place(x=50, y=50)


    def centerWindow(self):
        w = 500
        h = 500

        sw = self.parent.winfo_screenwidth()
        sh = self.parent.winfo_screenheight()

        x = (sw - w) / 2
        y = (sh - h) / 2
        self.parent.geometry('%dx%d+%d+%d' % (w, h, x, y))

def talk():
    tts = ALProxy("ALTextToSpeech", IPNao, portNao)
    tts.say("Hello, world!")


def main():
    root = Tk()
    root.geometry("500x500+100+100")
    app = WizardFrame(root)
    root.mainloop()


if __name__ == '__main__':
    main()