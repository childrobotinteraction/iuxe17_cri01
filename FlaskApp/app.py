from flask import Flask, render_template, request
import json
app = Flask(__name__)
currentCat = "Sport"
""" run with python app.py"""
""" control shift r for refresh"""
@app.route("/")
def main():
    return render_template('index.html')

@app.route('/getmethod')
def get_javascript_data():
    with open('response.json') as data_file:
        pythondata = json.load(data_file)
    """pythondata = ["Hond", "Kat", "Konijn", "Cavia", "Hamster"]"""
    return json.dumps(pythondata)

@app.route('/getCategory')
def get_category():
    return currentCat

@app.route('/postmethod', methods = ['POST'])
def post_javascript_data():
    jsdata = request.form['javascript_data']
    global currentCat
    currentCat = "Huisdieren"
    print jsdata
    return jsdata

""" host = ipv4 adress, check it with ipconfig"""
if __name__ == "__main__":
    print "hello"
    app.run(host='145.94.199.202', port=5000)
