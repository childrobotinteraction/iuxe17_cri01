"""
How to use the script:
Go to your choreographe installtion directory. bin -> launch naoqi-bin.exe
To see the result in choreographe launch choreographe: connect to -> check "Use fixed port" and "Use fixed IP/hostname" and click "Select"
Run this script, you should then see "Hello, world!" in choreographe.
"""
from Tkinter import *
from Classes.Nao import *
from flask import Flask, render_template, request
import json
import thread
import time
import os

IPNao = "localhost"
portNao = 9559
localIP = "192.168.1.70"


def getTimeString():
    return time.strftime('%d_%m_%YT%H_%M_%S', time.localtime(time.time()))

if not os.path.exists("./log"):
        os.makedirs("./log")
logFile = file = open("./log/" + str(getTimeString()) + ".txt", 'w+')
#portNao = 54613


"""
TABLET CODE
"""
app = Flask(__name__)
currentCat = "None"
lastAnswer = "None"
lastAnswerId = 0
lastAnsweredCat = "None"
""" run with python app.py"""
""" control shift r for refresh"""


def writeToLog(text):
    try:
        logFile.write(str(getTimeString()) + ", " + str(text) + "\n")
    except:
        print("Couldn't write to log!")

@app.route("/")
def main():
    return render_template('index.html')

@app.route('/getmethod')
def get_javascript_data():
    with open('response.json') as data_file:
        pythondata = json.load(data_file)
    #pythondata = ["Hond", "Kat", "Konijn", "Cavia", "Hamster"]
    return json.dumps(pythondata)

@app.route('/getCategory')
def get_category():
    return currentCat

@app.route('/postmethod', methods = ['POST'])
def post_javascript_data():
    jsdata = request.form['javascript_data']
    global currentCat
    global lastAnsweredCat
    lastAnsweredCat = currentCat
    currentCat = "None"
    print "jsData: " + str(jsdata)
    global appGui
    for item in json.load(open("response.json"))["iuxe"]:
        if str(item["id"]) == jsdata:
            print "true"
            global lastAnswer
            global lastAnswerId
            lastAnswer = item["option"]
            lastAnswerId = item["id"]
            appGui.lastAnswerText.config(state="normal")
            appGui.lastAnswerText.delete('1.0', END)
            appGui.lastAnswerText.insert(END, lastAnswer)
            appGui.lastAnswerText.config(state="disabled")
            nao.nudgeText = getNudgeText()
            appGui.sel()
            writeToLog("Selected Answer: " + str(lastAnswer))
            print "lastanswer: " + str(lastAnswerId) + str(lastAnswer)

    return jsdata


"""
END TABLET CODE
"""

with open('response.json') as data_file:
    data = json.load(data_file)

nao = Nao(IPNao, portNao, writeToLog)

introText = "Hallo! Mijn naam is Baymax. Ik kom vandaag even een kort gesprek met jou houden. Je zal er wel rekening" \
            "mee moeten houden dat ik misschien niet alles versta. Daarom ga je me soms moeten helpen door een aantwoord" \
            "in te voeren op de tablet. Wat is jouw naam?"

endText = "Dit was het dan al weer. Ik vond het leuk om met je te praten. Vond jij dat ook?  \\pau=1000\\ Tot ziens!"

nameCompliment = "Oh, dat is een mooie naam!"

sportQuestion = "Welke sport vind jij leuk? Kies een sport op de tablet."
petQuestion = "Welk huisdier vind jij leuk? Kies een huisdier op de tablet."
countryQuestion = "Waarheen ga jij graag op vakantie? Kies een bestemming op de tablet."
lessonQuestion = "Welk schoolvak vind je het leukst? Kies een schoolvak op de tablet."
colorQuestion = "Welke kleur vind jij het mooist? Kies een kleur op de tablet."

sportResponseAgree = "Oh, ik doe die sport ook graag!" \
                     "Meestal ga ik het minstens een keer per week doen."
sportResponseNuance = "Dat is wel een goede sport, ik doe het soms als ik me verveel"
sportResponseDisagree = "Dat vind ik geen leuke sport, ik doe liever iets specialer zoals rotsklimmen."

outroText = "Het lijkt er op dat ons gesprek er op zit. Ik vond het heelf fijn jou te ontmoeten vandaag. Wat vond jij er van?"
outroResponsePositive = "Dat is leuk om te horen! Hopelijk zien we elkaar snel nog eens! Tot ziens!"
outroResponseNegative = "Dat vind ik jammer om te horen. Hopelijk kunnen ze mij verbeteren zodat je me wel leuk vindt en zien we elkaar later nog eens. Tot ziens!"

notOnTabletText = "Ik ken enkel wat op de tablet staat. Als jouw antwoord niet op de tablet staat, kan je dan het best antwoord uit de opties op de tablet kiezen?"


nudgeAnswers = {"Sport": "Kun je meer vertellen over <answer>?",
                "Huisdieren": "Waarom vind je een <answer> leuk?",
                "Kleuren": "Wat vind je mooi aan <answer>?",
                "Vakantie": "Wat vind je leuk aan <answer>?",
                "Vakken": "Waarom vind je <answer> leuk?"}

type_response = ""
global_story = ""

def replaceNudgeText(string):
    global lastAnswer
    string = string.replace("<answer>", lastAnswer)
    print("strng after replace: " + string)
    print("lastanswer: " + lastAnswer)
    return string

def getNudgeText():
    global lastAnsweredCat
    return replaceNudgeText(nudgeAnswers[lastAnsweredCat])

class WizardFrame(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent, background="white")
        self.maxColumns = 4
        self.columns = 0
        self.rows = 0
        self.currentcolumnsinrow = -1 # 0 is first row so -1 is no rows
        self.width = 660
        self.height = 250

        self.parent = parent

        self.parent.title("Wizard of Oz")
        self.grid()
        self.centerWindow()

        #self.id_button = 1

        self.addRadioButtonsForStyle()

        #Buttons for introduction
        self.introButton = self.addAnimatedSayButton("Intro", introText)
        self.nameComplimentButton = self.addAnimatedSayButton("Compliment name", nameCompliment)

        #Buttons for Q&A
        #self.sportQuestionButton = self.addAnimatedSayButton("Sports Question", sportQuestion, True)
        #self.petQuestionButton = self.addAnimatedSayButton("Pet Question", petQuestion)
        #self.colorQuestionButton = self.addAnimatedSayButton("Color Question", colorQuestion)
        #self.countryQuestionButton = self.addAnimatedSayButton("Country Question", countryQuestion)
        #self.lessonQuestionButton = self.addAnimatedSayButton("Lesson Question", lessonQuestion)

        self.addSportsButtonQuestions()
        self.addPetButtonQuestions()
        self.addColorButtonQuestions()
        self.addLessonButtonQuestions()
        self.addCountryButtonQuestions()
        self.answerButton = self.addAnimatedSayStoryButton("Response", global_story, True)

        self.addNudgeCategoryButton()

        #Buttons for outro
        self.outroButton = self.addAnimatedSayButton("Outro", outroText, startNewRow=True)
        self.outroResponsePositive = self.addAnimatedSayButton("response positive", outroResponsePositive)
        self.outroResponseNegative = self.addAnimatedSayButton("response negative", outroResponseNegative, endRow=True)

        self.addEmergencyButtons()
        self.addLastAnswerText()

    def textBoxClear(self, event):
        self.emergencyText.delete(1.0, END)


    """Add widget (button, text box, ...) to the GUI in the next free position
    if startNewRow = True then the widget will be placed on a new row.
    if endRow = True then the row will be ended after adding the widget and it will
    it will be the last widget on that row."""
    def addWidget(self, widget, startNewRow=False, endRow=False):
        row, column = self.getColumnAndRowToAdd(startNewRow, endRow)
        widget.grid(row=row, column=column, sticky=N+S+E+W)

    def addNudgeCategoryButton(self):
        self.nudgeCategoryButton = Button(self, text="Nudge Category" ,command = lambda: nao.nudgeSay())
        self.addWidget(self.nudgeCategoryButton, endRow=True)

    def addLastAnswerText(self):
        self.lastAnswerText = Text(self, height=2, width=30)
        self.lastAnswerText.insert(END, lastAnswer)
        self.lastAnswerText.config(state='disabled')
        self.addWidget(self.lastAnswerText, startNewRow=True, endRow=True)


    def getColumnAndRowToAdd(self, startNewRow, endRow):
        self.currentcolumnsinrow += 1
        if self.currentcolumnsinrow > self.maxColumns or startNewRow:
            self.rows += 1
            self.currentcolumnsinrow = 0
        row = self.rows
        column = self.currentcolumnsinrow
        if endRow:
            self.currentcolumnsinrow = self.maxColumns
        return (row, column)

    def addRadioButtonsForStyle(self):
        self.v = IntVar()
        self.agreeChoice = Radiobutton(self, text="Agree", variable=self.v, value=1, command=lambda: self.sel())
        self.addWidget(self.agreeChoice)
        self.nuancedChoice = Radiobutton(self, text="Nuanced", variable=self.v, value=2, command=lambda: self.sel())
        self.addWidget(self.nuancedChoice, endRow=True)

    def sel(self):
        global lastAnswerId
        self.type_response = str(self.v.get())
        selection = "You selected the option " + self.type_response
        writeToLog("Option selected: " + str(self.type_response))
        print selection
        lastAnswerId = lastAnswerId - 1
        story = self.getResponse(lastAnswerId)
        nao.responseText= story

        print story

    def sportQuestionAction(self):
        global currentCat
        currentCat = "Sport"
        nao.animatedSay(sportQuestion)

    def addSportsButtonQuestions(self):
        self.sportQuestionButton = Button(self, text="Sports Question",
                                          command=lambda: self.sportQuestionAction())
        self.addWidget(self.sportQuestionButton, True)

    def petQuestionAction(self):
        global currentCat
        currentCat = "Huisdieren"
        nao.animatedSay(petQuestion)

    def addPetButtonQuestions(self):
        self.petQuestionButton = Button(self, text="Pet Question",
                                          command=lambda: self.petQuestionAction())
        self.addWidget(self.petQuestionButton)

    def colorQuestionAction(self):
        global currentCat
        currentCat = "Kleuren"
        nao.animatedSay(colorQuestion)

    def addColorButtonQuestions(self):
        self.colorQuestionButton = Button(self, text="Color Question",
                                          command=lambda: self.colorQuestionAction())
        self.addWidget(self.colorQuestionButton)

    def countryQuestionAction(self):
        global currentCat
        currentCat = "Vakantie"
        nao.animatedSay(countryQuestion)

    def addCountryButtonQuestions(self):
        self.countryQuestionButton = Button(self, text="Country Question",
                                          command=lambda: self.countryQuestionAction())
        self.addWidget(self.countryQuestionButton)

    def lessonQuestionAction(self):
        global currentCat
        currentCat = "Vakken"
        nao.animatedSay(lessonQuestion)

    def addLessonButtonQuestions(self):
        self.lessonQuestionButton = Button(self, text="Lesson Question",
                                          command=lambda: self.lessonQuestionAction())
        self.addWidget(self.lessonQuestionButton)


    def addAnimatedSayButton(self, title, text, startNewRow=False, *args, **kwargs):
        button = Button(self, text=title ,command = lambda: nao.animatedSay(text))
        self.addWidget(button, startNewRow, *args, **kwargs)
        return button

    def addAnimatedSayStoryButton(self, title, text, startNewRow=False):
        button = Button(self, text=title ,command = lambda: nao.responseSay())
        self.addWidget(button, startNewRow)
        return button

    def getResponse(self, id):
        if str(self.type_response) == "1":
            story = data["iuxe"][id]["story_explicit"]
        else:
            story = data["iuxe"][id]["story_nuanced"]
        return story

    def addEmergencyButtons(self):
        self.nudgeButton = Button(self, text="Nudge", command=lambda: nao.nudgeSayRandom())
        self.addWidget(self.nudgeButton, startNewRow=True)
        self.explanationButton = Button(self, text="Ask Explanation", command= lambda: nao.askFurtherExplanation())
        self.addWidget(self.explanationButton)
        self.enterOnTablet = self.addAnimatedSayButton("Enter on tablet", "Kan je het antwoord alsjeblieft op de tablet invoeren?")
        enterOnTablet = self.addAnimatedSayButton("Not on tablet", notOnTabletText)
        self.dontKnow = self.addAnimatedSayButton("Don't know", "Kun je echt geen reden verzinnen?")
        self.goOn = self.addAnimatedSayButton("Go On", "Ok, dan gaan we verder")

        #Emergency textbox/button
        self.emergencyText = Text(self, height=2, width=30)
        self.emergencyText.bind("<FocusIn>", lambda(event): self.textBoxClear(event))
        self.emergencyText.insert(END, "Enter text here")
        self.addWidget(self.emergencyText, startNewRow=True)
        self.emergencyButton = Button(self, text="Say message", command=self.sayEmergencyText)
        self.addWidget(self.emergencyButton, endRow=True)


    def selected(self):
        var = str(self.v.get())
        selection = "You selected the option " + var
        print selection

    def answerBasedOnChoice(var):
        if (var == 1):
            return "sportResponseAgree"
        else:
            return "sportResponseNuance"


    def sayEmergencyText(self):
        nao.animatedSay(str(self.emergencyText.get("1.0", END)))

    def centerWindow(self):
        w = self.width
        h = self.height

        sw = self.parent.winfo_screenwidth()
        sh = self.parent.winfo_screenheight()

        x = (sw - w) / 2
        y = (sh - h) / 2
        self.parent.geometry('%dx%d+%d+%d' % (w, h, x, y))

    #TODO: make it work with table app
    def getQuestionAnswer(self):
        return "Cat"

    def addResponseText(self):
        self.responseText = Text(self, height=2, width = 30)
        self.responseText.insert(END, "Response will be visible here")





def flaskThread():
    #app.run(host='192.168.1.70', port=5000)
    app.run(host=localIP, port=3000)


def mainGui():
    root = Tk()
    global appGui
    appGui = WizardFrame(root)
    root.mainloop()


""" host = ipv4 adress, check it with ipconfig"""
if __name__ == "__main__":
    print "hello"
    thread.start_new_thread(flaskThread,())
    mainGui()