from naoqi import ALProxy
import random

nudgeOptions = ["mhm", "ja", "ok"]
explanationOptions = ["Kan je me daar wat meer over vertellen?", "Daar wil ik wel wat meer over horen!"]

class Nao():
    def __init__(self, IP, port, writeToLogFunction):
        self.IP = IP
        self.port = port
        self.text2speech = ALProxy("ALTextToSpeech", IP, port)
        self.animatedSpeech = ALProxy('ALAnimatedSpeech', IP, port)
        self.motion = ALProxy("ALMotion", IP, port)
        self.posture = ALProxy("ALRobotPosture", IP, port)
        self.text2speech.setLanguage("Dutch")
        self.nudgeText = ""
        self.responseText = ""
        self.writeToLogFunction = writeToLogFunction
        self.motion.wakeUp()
        self.standUp()
        # Set bodylanguagemode to contextual mode
        self.animatedSpeech.setBodyLanguageMode(2)

    # Use animated speech to say sentence with optional configuration parameter
    def animatedSay(self, sentence, configuration=None):
        self.standUp()
        self.writeToLogFunction("AnimatedSay: " + str(sentence))
        self.animatedSpeech.say(sentence, configuration)

    def nudgeSayRandom(self):
        self.animatedSay(random.choice(nudgeOptions))

    def askFurtherExplanation(self):
        self.animatedSay(random.choice(explanationOptions))

    def standUp(self, speed=1.0):
        self.writeToLogFunction("Posture: Stand")
        self.posture.goToPosture("Stand", speed)

    def nudgeSay(self):
        self.animatedSay(str(self.nudgeText))

    def responseSay(self):
        self.animatedSay(str(self.responseText))