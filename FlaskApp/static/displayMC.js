var category = "None";
var previousCat = "None";
var changed = false;
var itemsArray = [];

$( document ).ready(function() {
    // Get the data
    $.get("/getmethod", function(data) {
        itemsArray = $.parseJSON(data).iuxe;
    })
});

/**
 * Sends id of selected answer to the server
 * @param id - id of selected answer
 */
function sendAnswerId(id) {
    // Remove all buttons and set current category to None
    $('.button').remove();
    previousCat = category;
    category = "None";
    $.post("/postmethod", {
        javascript_data: id
    });
}

/**
 * Periodically check if a new question has been selected
 */
setInterval(
    function() {
        // Get the current category of questions
        $.get("/getCategory", function(categoryName){
            // If it is a new category
            if (category != categoryName) {
                previousCat = category;
                category = categoryName;
                changed = true;
                $('.button').remove();
            }
            // If nothing changed, go back
            if (!changed) {
                return;
            }
            // If category changed create new buttons
            itemsArray.forEach(function(entry) {
                if (entry.category == category) {
                    const button = document.createElement("BUTTON");
                    const text = document.createTextNode(entry.option);
                    button.appendChild(text);
                    button.className = "button";
                    button.onclick = function() { sendAnswerId(entry.id) };
                    document.getElementById("toAppendTo").appendChild(button);
                }
            });
            changed = false;
        });
    },
    500
);